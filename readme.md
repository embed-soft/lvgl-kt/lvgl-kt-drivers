# LVGL KT Drivers (lvglkt-drivers)

A Kotlin Native library that provides Kotlin bindings to the [LV Drivers library](https://github.com/lvgl/lv_drivers). 
This library depends on Kotlin Native (currently in beta), and 
[LVGL KT Core](https://gitlab.com/embed-soft/lvgl-kt/lvgl-kt-core). Below is the key functionality provided by this 
library:

- Creation and management of input devices
- Input utilities (includes functions for setting up a Mouse and/or Touchscreen)
- Theming
- Basic display management

The following Kotlin Native targets are supported:
- linuxX64
- linuxArm32Hfp


## Requirements

The following is required by the library in order to use it:

- Kotlin Native 1.7.21 or later
- Gradle 6.9.2 or later
- LV Drivers 8.3

Note that the **liblv_drivers.a** file (a static library file) **MUST** be generated for a Kotlin Native program using 
this library, from the LV Drivers source code via [cmake](https://cmake.org/). Alternatively an existing 
**liblv_drivers.a** file can be acquired from the 
[Counter sample](https://gitlab.com/embed-soft/lvgl-kt/samples/counter-sample/-/tree/master/lib).


## Usage

To use this library in a Kotlin Native program using Gradle add the library as a dependency. Add the following to
the build file: `implementation(io.gitlab.embed-soft:lvglkt-drivers:0.4.0)`


## Input Devices

Various input devices can be used in a Kotlin Native program using this library. Initialize the **Evdev** system 
with the`initEvdev` function **before** using an input device. To use the Touchscreen call the `setupTouchscreen` 
function. If using a Mouse call the `setupMouse` function. All functions come from
[inputUtils.kt](src/commonMain/kotlin/io.gitlab.embedSoft.lvglKt.drivers/input/inputUtils.kt).
