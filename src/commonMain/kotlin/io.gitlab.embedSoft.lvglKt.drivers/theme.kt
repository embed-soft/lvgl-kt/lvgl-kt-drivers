package io.gitlab.embedSoft.lvglKt.drivers

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.styling.Font

/** Represents a collection of styles that are applied to a [display][Display]. */
public expect class Theme {
    /**
     * Changes the parent.
     * @param parent The theme to use as the parent.
     */
    public fun setParent(parent: Theme)

    /**
     * Changes the callback used for handling the apply event.
     * @param callback The callback to use.
     */
    public fun setApplyCallback(callback: ThemeCallback)

    public companion object {
        /**
         * Retrieves the primary color used for [obj].
         * @param obj The object to use.
         * @return The primary color.
         */
        public fun getPrimaryColor(obj: LvglObjectBase): Color

        /**
         * Retrieves the secondary color used for [obj].
         * @param obj The object to use.
         * @return The secondary color.
         */
        public fun getSecondaryColor(obj: LvglObjectBase): Color

        /**
         * Retrieves the small sized font used for [obj].
         * @param obj The object to use.
         * @return The small font.
         */
        public fun getSmallFont(obj: LvglObjectBase): Font

        /**
         * Retrieves the average (normal) sized font used for [obj].
         * @param obj The object to use.
         * @return The normal font.
         */
        public fun getNormalFont(obj: LvglObjectBase): Font

        /**
         * Retrieves the large sized font used for [obj].
         * @param obj The object to use.
         * @return The large font.
         */
        public fun getLargeFont(obj: LvglObjectBase): Font

        /**
         * Creates a new theme.
         * @return A new theme.
         */
        public fun create(
            display: Display,
            primaryColor: Color,
            secondaryColor: Color,
            dark: Boolean,
            vararg fonts: Font
        ): Theme
    }
}