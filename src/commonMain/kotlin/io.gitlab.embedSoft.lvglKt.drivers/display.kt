package io.gitlab.embedSoft.lvglKt.drivers

import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDriver
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/** Represents a physical display device (eg a Touchscreen). */
@Suppress("VARIABLE_IN_SINGLETON_WITHOUT_THREAD_LOCAL")
public expect class Display {
    /** The active screen for the display. */
    public val activeScreen: LvglObjectBase
    /** The previous screen. Only used during screen transitions. Will be *null* if the screen isn't used. */
    public val prevScreen: LvglObjectBase?
    /** Return with the top layer. Same on every screen and it is above the normal screen layer. */
    public val topLayer: LvglObjectBase
    /** Return with the system layer. Same on every screen and it is above the normal screen and the top layer. */
    public val sysLayer: LvglObjectBase
    /** The elapsed time (in ms) since last user activity on a display (e.g. click). */
    public val inactiveTime: UInt
    /** The theme that is applied to this display. */
    public var theme: Theme

    /** Manually trigger an activity on this display. */
    public fun triggerActivity()

    /** Clean any CPU cache that is related to this display. */
    public fun cleanCpuCache()

    /** Make a screen active. */
    public fun loadScreen(screen: LvglObjectBase)

    /**
     * Set the background color of the display.
     * @param color The color of the background.
     */
    public fun setBackgroundColor(color: Color)

    /**
     * Set the opacity of the background.
     * @param opacity The opacity from *0* to *255*.
     */
    public fun setBackgroundOpacity(opacity: UByte)

    /**
     * Scale the given number of [pixels] (a distance or size) relative to a 160 DPI display considering the DPI of
     * the default display. It ensures that e.g. `lv_dpx(100)` will have the same physical size regardless to the
     * DPI of the display.
     * @param pixels The number of pixels to scale.
     * @return The scale using the following formula: `pixels x currentDpi / 160`
     */
    public fun scalePixels(pixels: Short): Short

    /**
     * Set the background image of the display.
     * @param imageSrc The path to the image file.
     */
    public fun setBackgroundImage(imageSrc: StringReference)

    public companion object {
        /** The current display. In many cases this will be the primary display. */
        public val currentDisplay: Display
        /** The current theme applied to the [current display][currentDisplay]. */
        public var currentTheme: Theme
    }
}

/**
 * Scale the given number of [pixels] (a distance or size) relative to a 160 DPI display considering the DPI of
 * the default display. It ensures that e.g. `lv_dpx(100)` will have the same physical size regardless to the
 * DPI of the display.
 * @param pixels The number of pixels to scale.
 * @return The scale using the following formula: `pixels x currentDpi / 160`
 */
public expect fun scalePixels(pixels: Short): Short

/**
 * Register an initialized display driver. Automatically set the first display as active.
 * @return The display after registration.
 */
public expect fun DisplayDriver.register(): Display
