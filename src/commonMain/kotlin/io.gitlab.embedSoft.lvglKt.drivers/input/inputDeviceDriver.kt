package io.gitlab.embedSoft.lvglKt.drivers.input

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Represents an input device driver, which could be a Touchscreen, Mouse etc. */
public expect class InputDeviceDriver : Closable {
    /** Handles the read event when data has been read from an input device. */
    public var onRead: (driver: InputDeviceDriver, data: InputDeviceData) -> Unit
    /** Input device type (eg `InputDeviceType.POINTER`). */
    public var type: InputDeviceType

    public companion object {
        /**
         * Creates a new [InputDeviceDriver].
         * @return The new [InputDeviceDriver] instance.
         */
        public fun create(): InputDeviceDriver
    }

    /**
     * Registers the [InputDeviceDriver].
     * @return The input device.
     */
    public fun register(): InputDevice

    /**
     * Reads the input device.
     * @param data The data read from the input device.
     */
    public fun readInputDevice(data: InputDeviceData)
}
