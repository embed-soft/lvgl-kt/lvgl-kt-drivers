package io.gitlab.embedSoft.lvglKt.drivers.input

public enum class InputDeviceType {
    /** Uninitialized state. */
    NONE,
    /** Touch pad, mouse, external button. */
    POINTER,
    /** Keypad or keyboard. */
    KEYPAD,
    /** External (hardware button) which is assigned to a specific point of the screen. */
    BUTTON,
    /** Encoder with only Left, Right turn and a Button. */
    ENCODER
}
