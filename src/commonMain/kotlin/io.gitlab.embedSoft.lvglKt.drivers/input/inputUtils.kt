package io.gitlab.embedSoft.lvglKt.drivers.input

/**
 * Sets up the Mouse to be used as an input device.
 * @param cursorImg The path to the image file, or symbol (eg `LV_SYMBOL_GPS`) to be used as the Mouse cursor.
 * @return A Pair that contains the following:
 * 1. Input device driver
 * 2. Input device that represents the Mouse.
 */
public expect fun setupMouse(cursorImg: String): Pair<InputDeviceDriver, InputDevice>

/**
 * Sets up the Touch Screen as an input device.
 * @return A Pair that contains the following:
 * 1. Input device driver
 * 2. Input device that represents the Touch Screen.
 */
public expect fun setupTouchScreen(): Pair<InputDeviceDriver, InputDevice>

/** Initializes the **Evdev** system (part of the Linux Kernel) to ensure input devices can be used. */
public expect fun initEvdev()
