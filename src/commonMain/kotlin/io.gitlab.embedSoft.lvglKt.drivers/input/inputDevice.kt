package io.gitlab.embedSoft.lvglKt.drivers.input

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase

/** Represents an input device (Mouse, Touchscreen etc). */
public expect class InputDevice {
    /**
     * Changes the cursor used by the input device.
     * @param cursor The cursor to use, or *null* for no cursor.
     */
    public fun setCursor(cursor: LvglObjectBase?)
}
