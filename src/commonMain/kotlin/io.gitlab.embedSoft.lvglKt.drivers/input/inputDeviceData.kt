package io.gitlab.embedSoft.lvglKt.drivers.input

/** Contains data for the input device. */
public expect class InputDeviceData {
    /** Button identifier. */
    public var btnId: UInt
    /** Key. */
    public var key: UInt
    /** Whether to continue reading the input device. */
    public var continueReading: Boolean
    /** Encode difference. */
    public var encodeDiff: Short
    /** The input device state. */
    public var state: UInt
}
