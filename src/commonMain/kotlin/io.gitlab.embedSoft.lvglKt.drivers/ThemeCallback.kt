package io.gitlab.embedSoft.lvglKt.drivers

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase

/** Represents a theme callback. */
public expect class ThemeCallback : Closable {
    public companion object {
        /**
         * Creates a new [ThemeCallback] object.
         * @param callback The callback to use.
         * @param userData User data to pass to the [callback]. Will be *null* if empty.
         * @return A new [ThemeCallback].
         */
        public fun create(
            callback: (theme: Theme, obj: LvglObjectBase, userData: Any?) -> Unit,
            userData: Any?
        ): ThemeCallback
    }
}
