package io.gitlab.embedSoft.lvglKt.drivers

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_theme_apply_cb_t
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.toLvglObject
import kotlinx.cinterop.*

public actual class ThemeCallback private constructor(
    public val callback: (Theme, LvglObjectBase, Any?) -> Unit,
    public val userData: Any?
) : Closable {
    internal val stableRef = StableRef.create(this)
    public val lvThemeApplyCbPtr: lv_theme_apply_cb_t = staticCFunction { theme, obj ->
        val tmpTheme = theme.toTheme()
        val themeCallback = theme?.pointed?.user_data?.asStableRef<ThemeCallback>()?.get()
        themeCallback?.callback?.invoke(tmpTheme, obj.toLvglObject(), themeCallback.userData)
    }

    public actual companion object {
        public actual fun create(
            callback: (Theme, LvglObjectBase, Any?) -> Unit,
            userData: Any?
        ): ThemeCallback = ThemeCallback(callback, userData)
    }

    override fun close() {
        stableRef.dispose()
    }
}
