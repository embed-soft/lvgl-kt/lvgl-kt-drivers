package io.gitlab.embedSoft.lvglKt.drivers.input

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_indev_set_cursor
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_indev_t
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import kotlinx.cinterop.CPointer

public actual class InputDevice private constructor(ptr: CPointer<lv_indev_t>?){
    public val lvIndevPtr: CPointer<lv_indev_t>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<lv_indev_t>?): InputDevice = InputDevice(ptr)
    }

    public actual fun setCursor(cursor: LvglObjectBase?) {
        lv_indev_set_cursor(lvIndevPtr, cursor?.lvObjPtr)
    }
}

public fun CPointer<lv_indev_t>?.toInputDevice(): InputDevice = InputDevice.fromPointer(this)
