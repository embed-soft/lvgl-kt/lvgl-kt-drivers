package io.gitlab.embedSoft.lvglKt.drivers.input

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_indev_type_t

public fun lv_indev_type_t.toInputDeviceType(): InputDeviceType = when (this) {
    lv_indev_type_t.LV_INDEV_TYPE_KEYPAD -> InputDeviceType.KEYPAD
    lv_indev_type_t.LV_INDEV_TYPE_NONE -> InputDeviceType.NONE
    lv_indev_type_t.LV_INDEV_TYPE_POINTER -> InputDeviceType.POINTER
    lv_indev_type_t.LV_INDEV_TYPE_ENCODER -> InputDeviceType.ENCODER
    lv_indev_type_t.LV_INDEV_TYPE_BUTTON -> InputDeviceType.BUTTON
    else -> throw IllegalStateException("Unrecognised value.")
}

public fun InputDeviceType.toLvIndevType(): lv_indev_type_t = when (this) {
    InputDeviceType.POINTER -> lv_indev_type_t.LV_INDEV_TYPE_POINTER
    InputDeviceType.ENCODER -> lv_indev_type_t.LV_INDEV_TYPE_ENCODER
    InputDeviceType.KEYPAD -> lv_indev_type_t.LV_INDEV_TYPE_KEYPAD
    InputDeviceType.NONE -> lv_indev_type_t.LV_INDEV_TYPE_NONE
    InputDeviceType.BUTTON -> lv_indev_type_t.LV_INDEV_TYPE_BUTTON
}
