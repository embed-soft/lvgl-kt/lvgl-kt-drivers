package io.gitlab.embedSoft.lvglKt.drivers.input

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_indev_drv_init
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_indev_drv_register
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_indev_drv_t
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_indev_type_t
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.drivers.clib.lvDrivers.evdev_read

public actual class InputDeviceDriver private constructor(ptr: CPointer<lv_indev_drv_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    private val stableRef = if (ptr == null) StableRef.create(this) else null
    public val lvIndevDrvPtr: CPointer<lv_indev_drv_t>? = ptr ?: createLvIndevDrvPtr()
    public actual var type: InputDeviceType
        get() = (lvIndevDrvPtr?.pointed?.type ?: lv_indev_type_t.LV_INDEV_TYPE_NONE).toInputDeviceType()
        set(value) {
            lvIndevDrvPtr?.pointed?.type = value.toLvIndevType()
        }
    public actual var onRead: (driver: InputDeviceDriver, data: InputDeviceData) -> Unit = { _, data ->
        readInputDevice(data)
    }

    private fun createLvIndevDrvPtr(): CPointer<lv_indev_drv_t>? {
        val result = arena?.alloc<lv_indev_drv_t>()?.ptr
        lv_indev_drv_init(result)
        return result
    }

    private fun setupCallbacks() {
        lvIndevDrvPtr?.pointed?.user_data = stableRef?.asCPointer()
        lvIndevDrvPtr?.pointed?.read_cb = staticCFunction { driver, data ->
            val tmp = driver?.pointed?.user_data?.asStableRef<InputDeviceDriver>()?.get()
            tmp?.onRead?.invoke(driver.toInputDeviceDriver(), data.toInputDeviceData())
        }
    }

    public actual companion object {
        public actual fun create(): InputDeviceDriver {
            val result = InputDeviceDriver()
            result.setupCallbacks()
            return result
        }

        public fun fromPointer(ptr: CPointer<lv_indev_drv_t>?): InputDeviceDriver = InputDeviceDriver(ptr)
    }

    public actual fun register(): InputDevice = InputDevice.fromPointer(lv_indev_drv_register(lvIndevDrvPtr))

    override fun close() {
        arena?.clear()
        stableRef?.dispose()
    }

    public actual fun readInputDevice(data: InputDeviceData) {
        evdev_read(lvIndevDrvPtr, data.lvIndevDataPtr)
    }
}

public fun CPointer<lv_indev_drv_t>?.toInputDeviceDriver(): InputDeviceDriver = InputDeviceDriver.fromPointer(this)
