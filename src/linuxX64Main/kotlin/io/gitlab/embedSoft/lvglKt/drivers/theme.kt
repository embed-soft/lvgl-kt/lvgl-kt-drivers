package io.gitlab.embedSoft.lvglKt.drivers

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.styling.*
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.drivers.clib.lvDrivers.lvglkt_theme_default_init
import io.gitlab.embedSoft.lvglKt.drivers.clib.lvDrivers.lvglkt_theme_get_color_primary

public actual class Theme private constructor(ptr: CPointer<lv_theme_t>?){
    public val lvThemePtr: CPointer<lv_theme_t>? = ptr

    public actual companion object {
        public fun fromPointer(ptr: CPointer<lv_theme_t>?): Theme = Theme(ptr)

        public actual fun create(
            display: Display,
            primaryColor: Color,
            secondaryColor: Color,
            dark: Boolean,
            vararg fonts: Font
        ): Theme = memScoped {
            val tmpArray = allocArray<lv_font_t>(fonts.size)
            fonts.forEachIndexed { pos, item -> item.lvFontPtr?.pointed?.copy(tmpArray[pos]) }
            val ptr = lvglkt_theme_default_init(
                disp = display.lvDispPtr,
                color_primary = primaryColor.toCValue().toLvglktDriversColor(),
                color_secondary = secondaryColor.toCValue().toLvglktDriversColor(),
                dark = dark,
                font = tmpArray
            )
            return fromPointer(ptr)
        }

        public actual fun getPrimaryColor(obj: LvglObjectBase): Color {
            val tmpColor = lvglkt_theme_get_color_primary(obj.lvObjPtr)
            val red = tmpColor.useContents { red }
            val green = tmpColor.useContents { green }
            val blue = tmpColor.useContents { blue }
            return colorFromRgb(red = red.toUByte(), green = green.toUByte(), blue = blue.toUByte())
        }

        public actual fun getSecondaryColor(obj: LvglObjectBase): Color {
            val tmpColor = lvglkt_theme_get_color_primary(obj.lvObjPtr)
            val red = tmpColor.useContents { red }
            val green = tmpColor.useContents { green }
            val blue = tmpColor.useContents { blue }
            return colorFromRgb(red = red.toUByte(), green = green.toUByte(), blue = blue.toUByte())
        }

        public actual fun getSmallFont(obj: LvglObjectBase): Font = lv_theme_get_font_small(obj.lvObjPtr).toFont()

        public actual fun getNormalFont(obj: LvglObjectBase): Font = lv_theme_get_font_normal(obj.lvObjPtr).toFont()

        public actual fun getLargeFont(obj: LvglObjectBase): Font = lv_theme_get_font_large(obj.lvObjPtr).toFont()
    }

    public actual fun setParent(parent: Theme) {
        lv_theme_set_parent(lvThemePtr, parent.lvThemePtr)
    }

    public actual fun setApplyCallback(callback: ThemeCallback) {
        lvThemePtr?.pointed?.user_data = callback.stableRef.asCPointer()
        lv_theme_set_apply_cb(lvThemePtr, callback.lvThemeApplyCbPtr)
    }
}

public fun CPointer<lv_theme_t>?.toTheme(): Theme = Theme.fromPointer(this)
