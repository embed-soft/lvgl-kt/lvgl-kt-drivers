package io.gitlab.embedSoft.lvglKt.drivers.input

import io.gitlab.embedSoft.lvglKt.core.Image
import io.gitlab.embedSoft.lvglKt.core.Screen

public actual fun setupMouse(cursorImg: String): Pair<InputDeviceDriver, InputDevice> {
    val inDevDriver = InputDeviceDriver.create()
    inDevDriver.type = InputDeviceType.POINTER
    // Register the input device so it can be used.
    val mouseInDev = inDevDriver.register()
    // Create an image for the cursor.
    val cursor = Image.create(Screen.activeScreen)
    cursor.setSrc(cursorImg)
    // Connect the image to the input device.
    mouseInDev.setCursor(cursor)
    return inDevDriver to mouseInDev
}

public actual fun setupTouchScreen(): Pair<InputDeviceDriver, InputDevice> {
    val inDevDriver = InputDeviceDriver.create()
    inDevDriver.type = InputDeviceType.POINTER
    // Register the input device so it can be used.
    val touchScreenInDev = inDevDriver.register()
    return inDevDriver to touchScreenInDev
}

public actual fun initEvdev() {
    throw UnsupportedOperationException("Evdev isn't supported with the linuxX64 target")
}
