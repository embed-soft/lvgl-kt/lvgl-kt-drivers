package io.gitlab.embedSoft.lvglKt.drivers.input

import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_indev_data_t

public actual class InputDeviceData private constructor(ptr: CPointer<lv_indev_data_t>?) {
    public val lvIndevDataPtr: CPointer<lv_indev_data_t>? = ptr
    public actual var btnId: UInt
        get() = lvIndevDataPtr?.pointed?.btn_id ?: 0u
        set(value) {
            lvIndevDataPtr?.pointed?.btn_id = value
        }
    public actual var key: UInt
        get() = lvIndevDataPtr?.pointed?.key ?: 0u
        set(value) {
            lvIndevDataPtr?.pointed?.key = value
        }
    public actual var continueReading: Boolean
        get() = lvIndevDataPtr?.pointed?.continue_reading ?: false
        set(value) {
            lvIndevDataPtr?.pointed?.continue_reading = value
        }
    public actual var encodeDiff: Short
        get() = lvIndevDataPtr?.pointed?.enc_diff ?: 0.toShort()
        set(value) {
            lvIndevDataPtr?.pointed?.enc_diff = value
        }
    public actual var state: UInt
        get() = lvIndevDataPtr?.pointed?.state ?: 0u
        set(value) {
            lvIndevDataPtr?.pointed?.state = value
        }

    public companion object {
        public fun fromPointer(ptr: CPointer<lv_indev_data_t>?): InputDeviceData = InputDeviceData(ptr)
    }
}

public fun CPointer<lv_indev_data_t>?.toInputDeviceData(): InputDeviceData = InputDeviceData.fromPointer(this)
