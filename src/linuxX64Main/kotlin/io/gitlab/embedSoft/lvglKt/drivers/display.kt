package io.gitlab.embedSoft.lvglKt.drivers

import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDriver
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.toLvglObject
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.styling.toCValue
import kotlinx.cinterop.CPointer
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.drivers.clib.lvDrivers.lvglkt_disp_set_bg_color

public actual class Display private constructor(ptr: CPointer<lv_disp_t>?) {
    public val lvDispPtr: CPointer<lv_disp_t>? = ptr
    public actual val activeScreen: LvglObjectBase
        get() = lv_disp_get_scr_act(lvDispPtr).toLvglObject()
    public actual val prevScreen: LvglObjectBase?
        get() = lv_disp_get_scr_prev(lvDispPtr)?.toLvglObject()
    public actual val topLayer: LvglObjectBase
        get() = lv_disp_get_layer_top(lvDispPtr).toLvglObject()
    public actual val sysLayer: LvglObjectBase
        get() = lv_disp_get_layer_sys(lvDispPtr).toLvglObject()
    public actual val inactiveTime: UInt
        get() = lv_disp_get_inactive_time(lvDispPtr)
    public actual var theme: Theme
        get() = lv_disp_get_theme(lvDispPtr).toTheme()
        set(value) {
            lv_disp_set_theme(lvDispPtr, value.lvThemePtr)
        }

    public actual fun loadScreen(screen: LvglObjectBase) {
        lv_disp_load_scr(screen.lvObjPtr)
    }

    public actual fun setBackgroundColor(color: Color) {
        val newColor = color.toCValue().toLvglktDriversColor()
        lvglkt_disp_set_bg_color(lvDispPtr, newColor)
    }

    public actual fun setBackgroundOpacity(opacity: UByte) {
        lv_disp_set_bg_opa(lvDispPtr, opacity)
    }

    public actual companion object {
        public actual val currentDisplay: Display
            get() = Display(lv_disp_get_default())
        public actual var currentTheme: Theme
            get() = lv_disp_get_theme(null).toTheme()
            set(value) {
                lv_disp_set_theme(null, value.lvThemePtr)
            }

        public fun fromPointer(ptr: CPointer<lv_disp_t>?): Display = Display(ptr)
    }

    public actual fun scalePixels(pixels: Short): Short = lv_disp_dpx(lvDispPtr, pixels)

    public actual fun setBackgroundImage(imageSrc: StringReference) {
        lv_disp_set_bg_image(lvDispPtr, imageSrc.ref)
    }

    public actual fun triggerActivity() {
        lv_disp_trig_activity(lvDispPtr)
    }

    public actual fun cleanCpuCache() {
        lv_disp_clean_dcache(lvDispPtr)
    }
}

public fun CPointer<lv_disp_t>?.toDisplay(): Display = Display.fromPointer(this)

public actual fun scalePixels(pixels: Short): Short = lv_dpx(pixels)

public actual fun DisplayDriver.register(): Display = lv_disp_drv_register(lvDispDrvPtr).toDisplay()
