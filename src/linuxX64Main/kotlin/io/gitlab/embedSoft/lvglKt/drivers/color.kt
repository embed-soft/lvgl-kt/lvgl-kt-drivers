package io.gitlab.embedSoft.lvglKt.drivers

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_color_t
import io.gitlab.embedSoft.lvglKt.drivers.clib.lvDrivers.lvglkt_drivers_color_t
import kotlinx.cinterop.*

internal fun CValue<lvglkt_color_t>.toLvglktDriversColor(): CValue<lvglkt_drivers_color_t> = memScoped {
    var tmpRed = 0u.toUShort()
    var tmpGreen = 0u.toUShort()
    var tmpBlue = 0u.toUShort()
    var tmpFull = 0u.toUShort()
    useContents {
        tmpRed = red
        tmpGreen = green
        tmpBlue = blue
        tmpFull = full
    }
    val newColor = alloc<lvglkt_drivers_color_t>()
    newColor.red = tmpRed
    newColor.green = tmpGreen
    newColor.blue = tmpBlue
    newColor.full = tmpFull
    return newColor.readValue()
}
